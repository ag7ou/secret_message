'''
Secret message
for Udacity intro to Python course ex. 2
Gets a list of filenames in the current directory and
renames them trimming off any numeric characters
'''
import os
import string

def rename_files():
    files = os.listdir()
    for file in files:
        index = 0
        while file[index] in string.digits:
            index += 1
        os.rename(file, file[index:])
    return

rename_files()
